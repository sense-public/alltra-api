# วิธีการ Setup Project

- 1. npm install เพื่อทำการติดตั้ง node_modules package
- 2. สร้าง file ชื่อว่า .env
- 3. ทำการ copy ข้อมูลดังนี้

## Example

````shell
DB_NAME= database name
DB_HOST= database ip host
DB_PASSWORD= database password
DB_USER= database username
PORT= port ที่ต้องการให้ server run
FWLOG= (0) ไม่มีการส่ง syslog ต่อ (1) มีการส่ง syslog ต่อสำหรับ ที่ดิน
````

### เช่น 

````shell
DB_NAME=DOL_PDPA_DEV
DB_HOST=192.168.170.84
DB_PASSWORD=P@ssw0rd
DB_USER=users
PORT=8551
FWLOG=0
````
