const express = require('express');
const router = express.Router();
const api_controllers = require('../controllers/api_controllers');

router.post('/apicheckverify/apireq', api_controllers.api_activity);
router.post('/apidatain/apireqlog', api_controllers.api_datainlog);
router.get('/apidatain/apiimportdata', api_controllers.api_importdata);
router.post('/apidatain/apiimporttodb', api_controllers.api_importtodb);

module.exports = router;