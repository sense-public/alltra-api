const controller = {};
const { validationResult } = require('express-validator');
const path = require('path');
const fs = require("fs");
var db = require('../db/connectionDB')
const md5 = require('md5');
const sha1 = require('sha1');
const sha256 = require('sha256');
require('dotenv').config()

const winston = require('winston');
const { log } = require('console');
require('winston-syslog').Syslog;

const logger = winston.createLogger({
  levels: winston.config.syslog.levels,
  transports: [
    new winston.transports.Syslog(
      {
        host: `172.16.42.201`,
        port: 514,
        protocol: 'udp4',
        app_name: 'DOL_PDPA',
        pid: process.pid
      }
    )
  ]
});


function sethost(req) {
  var hostset = req.headers;
  var protocol = "http";
  if (hostset.hasOwnProperty("x-forwarded-proto")) {
    protocol = "https";
  }
  var host = protocol + "://" + req.headers.host;
  return host;
}
function addDate() {
  function addZero(i) {
    if (i < 10) { i = "0" + i }
    return i;
  }
  // const months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
  // let current_datetime = new Date()
  // let formatted_date = current_datetime.getFullYear() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getDate()
  // let current_time = new Date()
  // let formatted_time = addZero(current_time.getHours()) + ":" + addZero(current_time.getMinutes()) + ":" + addZero(current_time.getSeconds())
  // date = formatted_date + ' ' + formatted_time;
  // return date;
  const str = new Date().toLocaleString('sv-SE',{ timeZone: 'Asia/Jakarta' });
  return str;
}

function hashlog(msg) {
  const hashmd5 = md5(msg);
  const hashsha1 = sha1(msg);
  const hashsha256 = sha256(msg);
  var JSONHASH = { hashmd5: hashmd5, hashsha1: hashsha1, hashsha256: hashsha256 };
  return JSONHASH;
}

controller.api_activity_old = (req, res) => {
  var token = req.headers.token;
  var data = req.body;
  console.log(data);
  var checklength = data.personal.length;
  console.log("token : ", token);
  console.log("data : ", data.personal.length);
  if (checklength > 0) {
    var pushdatatest = [];
    for (k in data.personal) {
      for (s in data.personal[k]) {
        var newdata = [];

        var index1 = s;
        var index2 = parseInt(k);
        var personalval = data.personal[k][s];
        var multidata = data.dataname[k][s];
        multidata = multidata.split(",");
        var mulupdate = data.dataname[k][s];
        mulupdate = mulupdate.split(",");
        db.query(
          'SELECT * FROM TB_TR_PDPA_PATTERN INNER join TB_TR_PDPA_CLASSIFICATION on TB_TR_PDPA_CLASSIFICATION.pattern_id=TB_TR_PDPA_PATTERN.pattern_id INNER join TB_TR_APIVERIFY_USER as au on au.acc_id=TB_TR_PDPA_CLASSIFICATION.classify_user_access_info_process_outside_from_new_id INNER JOIN TB_TR_PDPA_EVENT_PROCESS as dp on dp.event_process_id=TB_TR_PDPA_CLASSIFICATION.event_process_id WHERE dp.event_process_code=?  and au.genkey=? and au.confirm="0";',
          [data.activity, token],
          (err, checkdata) => {
            // console.log(checkdata);
            if (checkdata.length > 0) {
              var multidatadoc_data = checkdata[0].doc_id_person_data;
              multidatadoc_data = multidatadoc_data.split(",");
              for (doc_data in multidatadoc_data) {
                db.query(
                  'SELECT ? as test ',
                  [doc_data],
                  (err, checkcount) => {
                    var t = parseInt(checkcount[0].test)
                    db.query(
                      'SELECT * FROM TB_TR_PDPA_PATTERN INNER join TB_TR_PDPA_CLASSIFICATION on TB_TR_PDPA_CLASSIFICATION.pattern_id=TB_TR_PDPA_PATTERN.pattern_id INNER join TB_TR_PDPA_DATA on TB_TR_PDPA_DATA.data_id=? INNER join TB_TR_APIVERIFY_USER as au on au.acc_id=TB_TR_PDPA_CLASSIFICATION.classify_user_access_info_process_outside_from_new_id INNER JOIN TB_TR_PDPA_EVENT_PROCESS as dp on dp.event_process_id=TB_TR_PDPA_CLASSIFICATION.event_process_id WHERE dp.event_process_code=? and TB_TR_PDPA_DATA.data_name=? and au.genkey=? and au.confirm="0";',
                      [multidatadoc_data[t], data.activity, personalval, token],
                      (err, check) => {
                        console.log(multidatadoc_data[t], data.activity, personalval, token);
                        var doc_num = 0;
                        if (check.length > 0) {
                          var datacheck =
                            check[0].classify_data_exception_or_unnecessary_filter_name;
                          datacheck = datacheck.split(",");
                          for (i in datacheck) {
                            for (x in multidata) {
                              if (multidata[x] == datacheck[i]) {
                                mulupdate[x] = "notsend";
                              }
                            }
                          }
                          for (p in mulupdate) {
                            if (mulupdate[p] != "notsend") {
                              newdata.push(mulupdate[p]);
                            }
                          }
                          if (check[0].classify_type_data_in_event_personal == 1) {
                            newdata = newtext(
                              newdata,
                              check[0].classify_type_data_in_event_personal_datamark
                            );
                          }
                          var test = {}
                          test[index1] = newdata[0].toString();
                          pushdatatest.push(test)
                          newdata = [];
                        }
                        if (data.personal.length == index2 + 1 && multidatadoc_data.length == t + 1) {
                          res.send({
                            status: "success-200",
                            value: data,
                            token: token,
                            dataresponse: pushdatatest,
                          });
                        }
                      }
                    );
                  });
              }

            } else if (data.personal.length == index2 + 1) {
              console.log("erorrrrrrrrrrrrr");
              res.send({ status: "ERROR-404", value: data, token: token });
            }
          });
      }
    }
  }
};

controller.api_activity_oldd = (req, res) => {
  var token = req.headers.token;
  var data = req.body;
  console.log(data);
  var checklength = data.personal.length;
  console.log("token : ", token);
  console.log("data : ", data.personal.length);
  var count_num = 0;
  if (typeof (data.personal) == "object") {
    // if (checklength > 0) {
      console.log("dsdasdas");
      var pushdatatest = [];
      for (k in data.personal) {
        var s = 'data' + (parseInt(k) + 1).toString();
        var newdata = [];
        const index1 = s;
        const index2 = parseInt(k);
        const personalval = data.personal[k][s];
        var multidata = data.dataname[k][s];
        console.log(data.dataname[k][s]);
        multidata = multidata.split(",");
        var mulupdate = data.dataname[k][s];
        mulupdate = mulupdate.split(",");
        const o = mulupdate
        db.query(
          'SELECT *,? as in1,? as in2 FROM TB_TR_PDPA_PATTERN INNER join TB_TR_PDPA_CLASSIFICATION on TB_TR_PDPA_CLASSIFICATION.pattern_id=TB_TR_PDPA_PATTERN.pattern_id INNER join TB_TR_APIVERIFY_USER as au on au.acc_id=TB_TR_PDPA_CLASSIFICATION.classify_user_access_info_process_outside_from_new_id INNER JOIN TB_TR_PDPA_EVENT_PROCESS as dp on dp.event_process_id=TB_TR_PDPA_CLASSIFICATION.event_process_id WHERE dp.event_process_code=?  and au.genkey=? and au.confirm="0";',
          [s, k, data.activity, token],
          (err, checkdata) => {
            if (checkdata.length > 0) {
              var multidatadoc_data = checkdata[0].doc_id_person_data;
              multidatadoc_data = multidatadoc_data.split(",");
              console.log(multidatadoc_data);
              console.log(index2, index1, personalval);
              for (doc_data in multidatadoc_data) {
                db.query(
                  'SELECT ? as test ',
                  [doc_data],
                  (err, checkcount) => {
                    var t = parseInt(checkcount[0].test)
                    console.log(index2, index1, personalval);
                    db.query(
                      'SELECT * FROM TB_TR_PDPA_PATTERN INNER join TB_TR_PDPA_CLASSIFICATION on TB_TR_PDPA_CLASSIFICATION.pattern_id=TB_TR_PDPA_PATTERN.pattern_id INNER join TB_TR_PDPA_DATA on TB_TR_PDPA_DATA.data_id=? INNER join TB_TR_APIVERIFY_USER as au on au.acc_id=TB_TR_PDPA_CLASSIFICATION.classify_user_access_info_process_outside_from_new_id INNER JOIN TB_TR_PDPA_EVENT_PROCESS as dp on dp.event_process_id=TB_TR_PDPA_CLASSIFICATION.event_process_id WHERE dp.event_process_code=? and TB_TR_PDPA_DATA.data_name=? and au.genkey=? and au.confirm="0";',
                      [multidatadoc_data[t], data.activity, personalval, token],
                      (err, check) => {
                        if (check.length > 0) {
                          count_num += 1;
                          for (p in o) {
                            newdata.push(o[p]);
                          }
                          if (check[0].classify_type_data_in_event_personal == 1) {
                            newdata = newtext(
                              newdata,
                              check[0].classify_type_data_in_event_personal_datamark
                            );
                          }
                          var test = {}
                          test[index1] = newdata[0].toString();
                          pushdatatest.push(test)
                          newdata = [];
                        }
                        if (data.personal.length == index2 + 1 && multidatadoc_data.length == t + 1) {
                          console.log('finish');
                          res.send({
                            status: "success-200",
                            value: data,
                            token: token,
                            dataresponse: pushdatatest,
                          });
                        }
                      }
                    );
                  });
              }

            } else if (data.personal.length == index2 + 1) {
              console.log("erorrrrrrrrrrrrr");
              res.send({ status: "ERROR-500 : data is null", value: data, token: token });
            }
          });
      }
    // }
    // else {
    //   res.send({ status: "ERROR-500 : data is not value", value: data, token: token });
    // }
  } else {
    res.send({ status: "ERROR-500 : data type error", value: data, token: token });
  }
};

controller.api_activity = (req, res) => {
  var token = req.headers.token;
  var data = req.body;
  console.log(data);
  console.log("token : ", token);
  console.log("data : ", data.dataname.length);
  if (typeof (data.personal) == "object" && typeof (data.dataname) == "object" ) {
    var pushdatatest = [];
    console.log("if 1");
    if (data.personal.length > 0) {
      console.log("if 2");
      if(data.personal.length == data.dataname.length ){
        var pushdatatest = [];
        db.query(
          'SELECT * FROM TB_TR_PDPA_PATTERN INNER join TB_TR_PDPA_CLASSIFICATION on TB_TR_PDPA_CLASSIFICATION.pattern_id=TB_TR_PDPA_PATTERN.pattern_id INNER join TB_TR_APIVERIFY_USER as au on FIND_IN_SET(au.acc_id, TB_TR_PDPA_CLASSIFICATION.classify_user_access_info_process_outside_from_new_id) > 0 INNER JOIN TB_TR_PDPA_EVENT_PROCESS as dp on dp.event_process_id=TB_TR_PDPA_CLASSIFICATION.event_process_id WHERE dp.event_process_code=?  and au.genkey=? and au.confirm="0" limit 1;',
          [ data.activity, token],
          (err, checkdata_isfound) => {
            if (checkdata_isfound.length > 0) {
              var count_num = 0;
              var count_num2 = 0;
                let multidatadoc_data = checkdata_isfound[0].doc_id_person_data;
                multidatadoc_data = multidatadoc_data.split(",");
              for (index_for1 in data.personal) {
                var newdata = [];
                const index1 = data.personal[index_for1].name;
                console.log(`for ${index_for1} :` ,data.personal[index_for1].name);
                let multidataname = data.personal[index_for1].name;
                multidataname = multidataname.split(",");
                let multidata = data.dataname[index_for1].data
                multidata = multidata.split(",");
                console.log(multidataname);
                console.log(multidata);
                console.log(multidatadoc_data);
                let o = multidata
                for (index_for2 in multidatadoc_data) {
                  console.log(count_num2);
                  count_num2++;
                  // console.log("dasdsa",index_for1,index_for2,count_num2);
                  db.query(
                    'SELECT * FROM TB_TR_PDPA_PATTERN INNER join TB_TR_PDPA_CLASSIFICATION on TB_TR_PDPA_CLASSIFICATION.pattern_id=TB_TR_PDPA_PATTERN.pattern_id INNER join TB_TR_PDPA_DATA on TB_TR_PDPA_DATA.data_id=? INNER join TB_TR_APIVERIFY_USER as au on FIND_IN_SET(au.acc_id, TB_TR_PDPA_CLASSIFICATION.classify_user_access_info_process_outside_from_new_id) > 0 INNER JOIN TB_TR_PDPA_EVENT_PROCESS as dp on dp.event_process_id=TB_TR_PDPA_CLASSIFICATION.event_process_id WHERE dp.event_process_code=? and TB_TR_PDPA_DATA.data_name=? and au.genkey=? and au.confirm="0";',
                    [multidatadoc_data[index_for2], data.activity, multidataname, token],
                    (err, datavalue) => {
                      // console.log(count_num);
                      count_num++;
                      console.log(count_num,count_num2);
                      if (datavalue.length > 0) {
                        console.log("if data found");
                        for (p in o) {
                          newdata.push(o[p]);
                        }
                        if (datavalue[0].classify_type_data_in_event_personal_datamark_check == 1) {
                          // newdata = newtext(
                          //   newdata,
                          //   datavalue[0].classify_type_data_in_event_personal_datamark
                          // );
                          console.log(newdata);
                          console.log(datavalue[0].classify_type_data_in_event_personal_datamark_total);
                           newdata = newtext(newdata, datavalue[0].classify_type_data_in_event_personal_datamark,datavalue[0].classify_type_data_in_event_personal_datamark_total);
                        }else{
                          newdata = newtext(newdata, "no mark",datavalue[0].classify_type_data_in_event_personal_datamark_total);
                          console.log(newdata);
                        }
                        var response = {}
                        response[index1] = newdata[0].toString();
                        pushdatatest.push(response)
                        newdata = [];
                      }
                      if (count_num == count_num2) {
                        console.log('finish');
                        res.send({
                          status: "success-200",
                          value: data,
                          token: token,
                          dataresponse: pushdatatest,
                        });
                      }
                    });
                }
              }
            }else{
              res.send({ status: "ERROR-401 : USER Unauthorized ", value: data, token: token });
            }
          });
      }else{
        res.send({ status: "ERROR-500 : DATA Personal and Dataname NOT Match", value: data, token: token });
      }
    }
    else {
      res.send({ status: "ERROR-500 : DATA is Personal null", value: data, token: token });
    }
  } else {
    res.send({ status: "ERROR-415 : DATA Unsupported Media Type", value: data, token: token });
  }
};


controller.api_datainlog = (req, res) => {
  const FWLOG = `${process.env.FWLOG}`;
  // console.log(`FWLOG :`,FWLOG);
  var data = req.body.data;
  // console.log(data);

  var multidata = []
  db.query(
    'SELECT * FROM TB_TR_DEVICE WHERE TB_TR_DEVICE.name="apidatainlog";',
    (err, device_check) => {
      if (device_check.length > 0) {
        var id_device = device_check[0].device_id;
          for (p in data) {
            var reshashlog = hashlog(data[p].msg);
            multidata.push([id_device, data[p].msg, data[p].date, data[p].file_name, reshashlog.hashmd5, reshashlog.hashsha1, reshashlog.hashsha256])
            if (FWLOG == 1) {
              logger.info(data[p].msg);
            }
          }

        db.query(
          'INSERT INTO TB_TR_LOG (device_id,msg,date,file_name,hash_md5,hash_sha1,hash_sha256) VALUES ?',
          [multidata],
          (err, checkdata) => {
            console.log(checkdata);
            if (checkdata != undefined) {
              res.send("INSERT success")
            } else {
              res.send("ERROR insert")
            }
          });

      } else {
        db.query(
          "INSERT INTO TB_TR_DEVICE (`device_id`, `status`, `de_type`, `data_type`, `sender`, `keep`, `rmfile`, `hostname`, `name`, `location_bu`, `backup`, `de_ip`, `eth`, `hash`, `image`) VALUES (NULL, 'ใช้งาน', 'อื่นๆ', 'ข้อมูลจราจรทางคอมพิวเตอร์จากการเชื่อมต่อเข้าถึงระบบเครือข่าย (Internet Access)', 'api', NULL, NULL, 'user', 'apidatainlog', NULL, NULL, '1.1.1.2', 'api', NULL, '1200px-Circle-icons-computer.svg.png');",
          (err, device) => {
            var id_device = device.insertId;
            // for (p in data) {
            //   multidata.push([id_device, data[p].msg, data[p].date, data[p].file_name])
            // }
            for (p in data) {
              var reshashlog = hashlog(data[p].msg);
              multidata.push([id_device, data[p].msg, data[p].date, data[p].file_name, reshashlog.hashmd5, reshashlog.hashsha1, reshashlog.hashsha256])
              if (FWLOG == 1) {
              console.log(`logger.info('`, data[p].msg, `');`);
              }
            }
            db.query(
              'INSERT INTO TB_TR_LOG (device_id,msg,date,file_name,hash_md5,hash_sha1,hash_sha256) VALUES ?',
              [multidata],
              (err, checkdata) => {
                console.log(checkdata);
                if (checkdata != undefined) {
                  res.send("INSERT success")
                } else {
                  res.send("ERROR insert")
                }
              });
          });

      }
    });

  // db.query(
  //   'INSERT INTO TB_TR_LOG SET ?',
  //   [data],
  //   (err, checkdata) => {
  //     console.log(checkdata);
  //     if (checkdata != undefined) {
  //       res.send("INSERT success")
  //     }else{
  //       res.send("ERROR insert")
  //     }
  //   });
};

controller.api_importdata = (req, res) => {
  var datasend = {};
  var test = [];
  var test2 = [];
  var test3 = [];
  db.query(
    'SELECT * FROM TB_TR_IMPORT_DATA',
    (err, dataimport) => {
      for (i in dataimport) {
        if (dataimport[i].columns == 0) {
          test.push(dataimport[i].value);
        } else if (dataimport[i].columns == 1) {
          test2.push(dataimport[i].value);
        } else if (dataimport[i].columns == 2) {
          test3.push(dataimport[i].value);
        }
      }
      datasend["name"] = test
      datasend["surename"] = test2
      datasend["idcard"] = test3
      res.send({ status: "success-200", source: dataimport[0].source, data: datasend })
    });

};

controller.api_importtodb = (req, res) => {
  var data = req.body;
  var datetime = addDate();
  var datareq = data.data;
  var countx = 0;
  var multidata = []
  db.query(
    'SELECT * FROM TB_TR_IMPORT where name=?', [data.name],
    (err, dataimport) => {
      var id = dataimport[0].ftp_id;
      var rename = 'api_' + makekeygen(16);
      if (dataimport.length > 0) {
        db.query(
          "INSERT INTO TB_TR_IMPORT_FILE (name, date, import_id, rname, status) VALUES (?, ?, ?, ?, '0');", [data.name, datetime, id, rename],
          (err, insert_im_file) => {
            // console.log(insert_im_file);
            if (insert_im_file != undefined) {
              var count = 0
              var rec = 0
              db.query(
                "SELECT * FROM TB_TR_PDPA_DATA",
                (err, checkdata) => {
                  for (a in checkdata) {
                    var namesplit = checkdata[a].data_code.replace("#", "");
                    var data_id = checkdata[a].data_id
                    var num1 = parseInt(a)
                    console.log(namesplit);
                    for (x in datareq) {
                      if (x == namesplit) {
                        var namedata = x
                        var value = datareq[x]
                        var id_insert = insert_im_file.insertId
                        for (y in value) {
                          var num2 = parseInt(y)
                          multidata.push([count, y, value[y], 'api', data.name, datetime, data_id, id_insert])
                        }
                        count += 1
                      }
                    }
                    if (num1 + 1 == checkdata.length) {
                      db.query(
                        "INSERT INTO `TB_TR_IMPORT_DATA` ( `columns`, `rows`, `value`, `type`, `source`, `date`, `doc_pdpa_data_id`, `import_file_id`) VALUES ?;"
                        , [multidata],
                        (err, insert_im_data) => {
                          res.send("INSERT SUCCESS")
                        });

                    }
                  }


                });
            }
          });
      } else {
        res.send("ERROR success")
      }
    });

};



function newtext(data, typeid ,amount) {
  var newdata = [];
  var data = [data]
  for (x in data) {
    var ar = [];
    for (z in data[x]) {
      var text = marktext2(data[x][z], typeid ,amount);

      ar.push(text);
    }
    newdata.push(ar);
  }
  return newdata;
}

// function newtext_not_image(data, typeid, amount) {
//   var newdata = [];
//   var data = [data]
//   for (x in data) {
//     var ar = [];
//     if (x == 0) {
//       newdata.push(data[x]);
//     } else {
//       for (z in data[x]) {
//         console.log(data[x][z], typeid, amount);
//         var text = marktext2(data[x][z], typeid, amount);
//         ar.push(text);
//       }
//       newdata.push(ar);
//     }
//   }
//   // console.log(newdata);
//   return newdata;
// }
function marktext(textlabel, type) {
  var text = "";
  var possible = textlabel;
  let count = 0;
  for (var i = 0; i < possible.length; i++) {
    if (i == 0 && type == 0) {
      text += possible[i].replace(possible[i], "*");
    } else if (i == 0 && type == 1) {
      text += possible[i].replace(possible[i], "*");
    } else if (i == possible.length - 1 && type == 1) {
      text += possible[i].replace(possible[i], "*");
    } else if (i == possible.length - 1 && type == 2) {
      text += possible[i].replace(possible[i], "*");
    } else if (type == 3) {
      text += possible[i].replace(possible[i], "*");
    } else {
      text += possible[i];
    }
  }
  return text;
}

function marktext2(textlabel, type, amount ) {
  var text = "";
  if (textlabel != undefined) {
		var textlabel_length = textlabel.length;
    
		//Check Amount
		if (amount >= (textlabel_length - 2)) {
			amount = textlabel_length - 2;
		}
		//Mask
		if (type == 0) { //type = 0 >> ซ่อนด้านหน้า
			//Mask with *
			for (var i = 0;i < (textlabel_length - amount);i++) {
				text += "*";
			}
			//Sub String
			text += textlabel.substr(textlabel_length - amount, amount);
		}
		else if (type == 1) { //type = 1 >> ซ่อนด้านหน้าและหลัง
			var index_start = Math.floor((textlabel_length - amount)/2);
			//Mask with *
			for (var i = 0;i < index_start;i++) {
				text += "*";
			}
			//Sub String
			text += textlabel.substr(index_start, amount);
			//Mask with *
			for (var i = 0;i < ((textlabel_length - amount) - index_start);i++) {
				text += "*";
			}
		}else if (type == 2){ //type = 2 >> ซ่อนด้านหลัง
			//Sub String
			text += textlabel.substr(0, amount);
			//Mask with *
			for (var i = 0;i < (textlabel_length - amount);i++) {
				text += "*";
			}
		}else if (type == 3){ //type = 3 >> ซ่อนทั้งหมด
			//Sub String
			// text += textlabel.substr(0, amount);
			//Mask with *
			for (var i = 0;i < (textlabel_length);i++) {
				text += "*";
			}
		}else{ // notmark
      text = textlabel;
    }
	}
  return text;
}

function imcsv(filepath) {
  var data = fs
    .readFileSync(filepath)
    .toString() // convert Buffer to string
    .split("\n") // split string to lines
    .map((e) => e.trim()) // remove white spaces for each line
    .map((e) => e.split(",").map((e) => e.trim())); // split each line to array
  return data;
}

function makekeygen(length) {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function filter_acc_id(target,multiname) {
 
  var pattern = ['.gif', '.png', '.jpg', '.jpeg', '.webp'];
  var value = 0;
  pattern.forEach(function (word) {
    value = value + target.includes(word);
  });
  return (value === 1)
}

module.exports = controller;