const express = require('express');
const body = require('body-parser');
const cookie = require('cookie-parser');
const session = require('express-session');
const mysql = require('mysql');
const connection = require('express-myconnection');
const path = require("path");
const fileUpload = require("express-fileupload");
const util = require("util");
const app = express();
const fs = require('fs');
const http = require('http');
const thai = require('node-datetime-thai');
// const session_id = require('express-session-id'); // npm get session_id no browser
const cors = require('cors');
require('dotenv').config()
app.use(cors());
app.use(fileUpload());
app.use(cookie());
app.use('/assets', express.static('assets')) // เเ
app.use('/dist', express.static('dist'))


const { check } = require('express-validator');
const { validationResult } = require('express-validator');
const multer = require('multer')

//app.use(express.static('public'));
app.use(express.static(path.join(__dirname, '/public')));
app.use(fileUpload());
//setting
app.use(express.static('public'));
app.set('view engine', 'ejs');
app.use(body.json({ limit: '50mb' }))
app.use(body.urlencoded({ limit: '50mb', extended: true }));
//app.set('views' , 'views');
app.set('views', path.join(__dirname, '/views'));
app.use(express.static(__dirname + '/public'))
    //middlewares

app.use(body.urlencoded({ extended: true }));
app.use(cookie());

app.use(session({
    secret: 'Passw0rd',
    resave: true,
    saveUninitialized: true,
    cookie: { maxAge: 60000000 }
}));

// apiToken
const apiRoute = require('./routes/apiRoute');
app.use('/', apiRoute);


const PORT = process.env.PORT;
app.listen(PORT, () => console.log(`Server Listen PORT ${PORT}`))

